/* global instantsearch */

import TypesenseInstantSearchAdapter from 'typesense-instantsearch-adapter';

const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
    server: {
        apiKey: 'YeeZQsXmrSZWOAX5mbblmi9LmPjXWYe9', // Be sure to use an API key that only allows searches, in production
        nodes: [
            {
                host: 'oiqv712b653mtxlap-1.a1.typesense.net',
                port: '443',
                protocol: 'https',
            },
        ],
    },
    // The following parameters are directly passed to Typesense's search API endpoint.
    //  So you can pass any parameters supported by the search endpoint below.
    //  queryBy is required.
    //  filterBy is managed and overridden by InstantSearch.js. To set it, you want to use one of the filter widgets like refinementList or use the `configure` widget.
    additionalSearchParameters: {
        queryBy: 'title, authors, publisher_name, published_place, call_number, series, keywords',
    },
});
const searchClient = typesenseInstantsearchAdapter.searchClient;

const search = instantsearch({
    searchClient,
    indexName: 'books',
});

search.addWidgets([
    instantsearch.widgets.searchBox({
        container: '#searchbox',
    }),

    instantsearch.widgets.configure({
        hitsPerPage: 8,
    }),
    instantsearch.widgets.hits({
        container: '#hits',
        templates: {
            item(item) {
                return `
        <div>
          <div class="hit-name">
            ${item._highlightResult.title.value}
          </div>
          <div class="hit-authors">
          ${item._highlightResult.authors.map(a => a.value).join(', ')}
          </div>
          <div class="hit-publication-year">${item.published_place}, ${item.publisher_name}</div>
        </div>
      `;
            },
        },
    }),
    instantsearch.widgets.pagination({
        container: '#pagination',
    }),
]);

search.start();
